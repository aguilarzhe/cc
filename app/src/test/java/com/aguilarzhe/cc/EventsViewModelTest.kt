package com.aguilarzhe.cc

import com.aguilarzhe.cc.ui.EventsViewModel
import com.aguilarzhe.cc.webservices.Model
import junit.framework.Assert
import org.junit.Test

class EventsViewModelTest{
    @Test
    fun convertResultToEvent_test(){
        val count = 1
        val topLabel = ""
        val middleLabel = ""
        val bottomLabel = ""
        val eventCount = 0
        val image = ""
        val targetId = 0
        val targetType = ""
        val entityId = 0
        val entityType = ""
        val startDate = 0L
        val rank = ""

        val resultArray = ArrayList<Model.Result>()
        resultArray.add(
                Model.Result(topLabel,
                        middleLabel,
                        bottomLabel,
                        eventCount,
                        image,
                        targetId,
                        targetType,
                        entityId,
                        entityType,
                        startDate,
                        rank
                ))

        val array = EventsViewModel.convertResultToEvent(resultArray.toTypedArray())

        Assert.assertEquals(count, array.count())
        val event = array.get(0)
        Assert.assertEquals(topLabel, event.topLabel)
        Assert.assertEquals(middleLabel, event.middleLabel)
        Assert.assertEquals(bottomLabel, event.bottomLabel)
        Assert.assertEquals(eventCount, event.eventCount)
        Assert.assertEquals(image, event.image)
        Assert.assertEquals(targetId, event.targetId)
        Assert.assertEquals(targetType, event.targetType)
        Assert.assertEquals(entityId, event.entityId)
        Assert.assertEquals(entityType, event.entityType)
        Assert.assertEquals(startDate, event.startDate)
        Assert.assertEquals(rank, event.rank)

    }
}