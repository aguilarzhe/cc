package com.aguilarzhe.cc.webservices

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

/**
 * Define the connection with services.
 */
interface CardRestConnection{
    companion object {
        fun create(): CardRestConnection {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .addConverterFactory(
                            GsonConverterFactory.create())
                    .baseUrl("https://webservices.vividseats.com/")
                    .build()

            return retrofit.create(CardRestConnection::class.java)
        }
    }

    /**
     * Define the parameters that will be sent to cards service
     */
    data class Request(var startDate: String, var endDate: String, var includeSuggested: Boolean)

    /**
     * Define the connection to cards service. The service requires a POST request, with the
     * values defined by {@link Request} class. The result it is an array of {@link Model.Result}
     * object.
     */
    @POST("/rest/mobile/v1/home/cards")
    fun eventsForDate(@Body request: Request) :
            Observable<Array<Model.Result>>
}

