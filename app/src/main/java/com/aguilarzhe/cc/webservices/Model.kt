package com.aguilarzhe.cc.webservices

/**
 * Define the model that will be respond by web service.
 */
object Model {
    // TODO Ask meaning of each value
    data class Result(val topLabel: String?,
                      val middleLabel: String?,
                      val bottomLabel: String?,
                      val eventCount: Int?,
                      val image: String?,
                      val targetId: Int?,
                      val targetType: String?,
                      val entityId: Int?,
                      val entityType: String?,
                      val startDate: Long?,
                      val rank: String?)
}