package com.aguilarzhe.cc.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

/**
 * Creates a room database to manage the Events' persistence.
 */
@Database( entities = arrayOf(Event::class), version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract fun eventDao() : EventDao

}