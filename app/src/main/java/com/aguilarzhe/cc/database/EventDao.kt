package com.aguilarzhe.cc.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import io.reactivex.Flowable

/**
 * Defines the data access object to retrieve data from event's table.
 */
// TODO Refactor method's names and delete not used methods
@Dao
interface EventDao{
    /**
     * Get a LiveData object with all available events, downloaded from web service.
     */
    @Query("SELECT * FROM event")
    fun getAll(): LiveData<List<Event>>

    @Query("SELECT * from event")
    fun getAllEvents(): Flowable<Event>

    /**
     * Insert a Event's List into table
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(events: ArrayList<Event>)

    /**
     * Delete a given event from table
     */
    @Delete
    fun delete(user: Event)

    @Delete
    fun deleteAll(events: ArrayList<Event>)
}