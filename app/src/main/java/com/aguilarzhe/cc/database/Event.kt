package com.aguilarzhe.cc.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.databinding.BaseObservable
import com.android.databinding.library.baseAdapters.BR
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Defines an object that store Event's values. An event can contain descriptions, counts, target
 * and entity types.
 */
@Entity
class Event: BaseObservable(){
    @ColumnInfo(name = "topLabel")
    var topLabel: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.event)
        }

    @ColumnInfo(name = "middleLabel")
    var middleLabel: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.event)
        }

    @ColumnInfo(name = "bottomLabel")
    var bottomLabel: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.event)
        }

    @ColumnInfo(name = "eventCount")
    var eventCount: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.event)
        }

    @ColumnInfo(name = "image")
    var image: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.event)
        }

    @ColumnInfo(name = "targetId")
    var targetId: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.event)
        }

    @ColumnInfo(name = "targetType")
    var targetType: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.event)
        }

    @PrimaryKey
    @ColumnInfo(name = "entityId")
    var entityId: Int? = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.event)
        }

    @ColumnInfo(name = "entityType")
    var entityType: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.event)
        }

    @ColumnInfo(name = "startDate")
    var startDate: Long? = Long.MAX_VALUE

    @ColumnInfo(name = "rank")
    var rank: String? = null
        set(value) {
            field = value
            notifyPropertyChanged(BR.event)
        }

    /**
     * Get a String object with the startDate formatted.
     */
    fun getStartDateStr() : String{
        val format = SimpleDateFormat.getDateInstance() // TODO: Define a custom formatter
        return format.format(Date(startDate!!)) // TODO: Validate that date isn't Long.MAX_VALUE
    }
}