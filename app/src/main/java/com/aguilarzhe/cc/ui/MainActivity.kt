package com.aguilarzhe.cc.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.aguilarzhe.cc.R
import com.aguilarzhe.cc.database.Event
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_main.*
import android.graphics.Bitmap
import android.databinding.BindingAdapter
import android.graphics.BitmapFactory
import android.support.v4.graphics.BitmapCompat
import android.widget.ImageView
import android.R.attr.src
import android.os.AsyncTask
import java.net.HttpURLConnection
import java.net.URL


/**
 * It shows the main view of the application. It displays a ViewPager which contains three tabs:
 * - Suggested. The events that are suggested to user.
 * - Viewed. The events that was seen by user.
 * - Favorites. The events that was marked favorite by user.
 *
 * The user can search an object by team, performer, event or venue with the search bar. Or he can
 * use the location button.
 *
 * User can view the settings activity, clicking the settings button.
 */
class MainActivity : AppCompatActivity() {
    /**
     * The view model object manages the connection with room database and services.
     */
    lateinit var viewModel : EventsViewModel

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(EventsViewModel::class.java)

        setSupportActionBar(toolbar)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter

        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))

        viewModel.onCreate()
    }

    override fun onStart() {
        super.onStart()

        viewModel.onStart()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1)
        }

        override fun getCount(): Int {
            // Show 3 total pages.
            return 3
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    // TODO: Generate different fragments to manage each tab
    class PlaceholderFragment : Fragment() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            val viewDataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false) as ViewDataBinding


            //viewData.section_label.text = getString(R.string.section_format, arguments?.getInt(ARG_SECTION_NUMBER))
            return viewDataBinding.root
        }

        override fun onStart() {
            super.onStart()
            val recyclerViewAdapter = EventBaseAdapter()
            recyclerView.layoutManager = LinearLayoutManager(context)
            recyclerView.adapter = recyclerViewAdapter

            val viewModelFragment = ViewModelProviders.of(this).get(EventsViewModel::class.java)
            viewModelFragment.eventLiveData.observe(this, object : Observer<List<Event>>{
                override fun onChanged(list: List<Event>?) {
                    recyclerViewAdapter.updateList(list!!)
                }
            })
        }

        companion object {
            /**
             * The fragment argument representing the section number for this
             * fragment.
             */
            private val ARG_SECTION_NUMBER = "section_number"

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(sectionNumber: Int): PlaceholderFragment {
                val fragment = PlaceholderFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.arguments = args
                return fragment
            }
        }
    }



    override fun onPause() {
        super.onPause()

        viewModel.onPause()
    }

    override fun onResume() {
        super.onResume()

        viewModel.onResume()
    }
}
