package com.aguilarzhe.cc.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.util.Log
import com.aguilarzhe.cc.webservices.CardRestConnection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import com.aguilarzhe.cc.database.AppDatabase
import android.arch.persistence.room.Room
import com.aguilarzhe.cc.database.Event
import com.aguilarzhe.cc.webservices.Model
import io.reactivex.disposables.CompositeDisposable
import kotlin.collections.ArrayList


class EventsViewModel(application: Application) : AndroidViewModel(application){
    var eventLiveData : LiveData<List<Event>>

    val connection by lazy {
        CardRestConnection.create()
    }

    val database by lazy {
            Room.databaseBuilder(application,
                AppDatabase::class.java, "database-name").build()
    }

    init {
        eventLiveData = database.eventDao().getAll()
    }

    var disposable: Disposable? = null
    var compositeDisposable :CompositeDisposable = CompositeDisposable()

    fun onCreate() {



        /*
        compositeDisposable.add(database.eventDao().getAllEvents()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ userName -> mUserName.setText(userName) },
                        { throwable -> Log.e(TAG, "Unable to update username", throwable) }))
                        */
    }

    fun onPause() {

        compositeDisposable.dispose()
    }


    fun onResume() {
    }

    fun onStart(){
        getCards(Calendar.getInstance().time)
    }
    fun onDestroy() {}



    fun getCards(endDate: Date){
        val request = CardRestConnection.Request("2018-08-04", "2018-08-07", true)

        compositeDisposable.add(connection.eventsForDate(request)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { result -> run{

                                    Thread(object : Runnable{
                                        override fun run() {
                                            database.eventDao().insertAll(convertResultToEvent(result))
                                        }
                                    }).start()
                                } },
                                {
                                    error -> Log.d("asd", error.message)
                                }
                        ))
    }
    companion object {
        fun convertResultToEvent(result: Array<Model.Result>): ArrayList<Event> {
            val array = ArrayList<Event>()
            for (r in result) {
                val event = Event()
                event.topLabel = r.topLabel ?: ""
                event.middleLabel = r.middleLabel ?: ""
                event.bottomLabel = r.bottomLabel ?: ""
                event.eventCount = r.eventCount ?: 0
                event.image = r.image
                event.entityId = r.entityId
                event.entityType = r.entityType
                event.targetId = r.targetId ?: 0
                event.targetType = r.targetType
                event.startDate = r.startDate
                event.rank = r.rank

                array.add(event)
            }

            return array
        }
    }
}