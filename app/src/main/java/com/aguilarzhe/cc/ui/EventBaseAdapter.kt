package com.aguilarzhe.cc.ui

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.aguilarzhe.cc.R
import com.aguilarzhe.cc.database.Event
import java.util.ArrayList

/**
 * Adapter that manage RecyclerView's data that it displays Events to user.
 */
class EventBaseAdapter : RecyclerView.Adapter<EventViewHolder>() {

    var list : List<Event> = ArrayList()

    /**
     * Update list object and notify that data set was changed.
     */
    fun updateList(list:List<Event>){
        this.list = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): EventViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
                layoutInflater, viewType, parent, false)

        return EventViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EventViewHolder,
                                  position: Int) {
        val event = getObjForPosition(position) as Event
        holder.bind(event)
    }

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    override fun getItemCount(): Int = list.count()

    protected fun getObjForPosition(position: Int): Any = list.get(position)

    protected fun getLayoutIdForPosition(position: Int): Int = R.layout.element_view
}