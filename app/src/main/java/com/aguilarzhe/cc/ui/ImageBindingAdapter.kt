package com.aguilarzhe.cc.ui

import android.databinding.BindingAdapter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.widget.ImageView
import io.reactivex.disposables.CompositeDisposable
import java.net.HttpURLConnection
import java.net.URL

class ImageBindingAdapter {

    companion object {
        var compositeDisposable : CompositeDisposable = CompositeDisposable()

        @JvmStatic
        @BindingAdapter("bind:imageBitmap")
        fun loadImage(iv: ImageView, bitmapStr: String) {

            object : AsyncTask<String, Void, Bitmap?>(){

                override fun doInBackground(vararg p0: String?): Bitmap? {
                    val url = URL(p0[0])
                    try {
                        val connection = url.openConnection() as HttpURLConnection
                        connection.setDoInput(true)
                        connection.connect()
                        val input = connection.getInputStream()
                        return BitmapFactory.decodeStream(input)
                    } catch (e: Exception) {
                        return null
                    }
                }

                override fun onPostExecute(result: Bitmap?) {
                    super.onPostExecute(result)
                    if(result != null) iv.setImageBitmap(result)
                }
            }.execute(bitmapStr)
        }
    }

}