package com.aguilarzhe.cc.ui

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import com.aguilarzhe.cc.BR
import com.aguilarzhe.cc.database.Event


class EventViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root){
    fun bind(event: Event) {
        binding.setVariable(BR.event, event)
        binding.executePendingBindings()
    }
}

